from online import get_currencies

# приветствие
# вывод результата
# мануал
# ввести исхолну валюту
# вести целевую валюту
# количество исходной валюты
# подсчет

def convert(amount, source, target, currencies):
    source_rate = currencies.get(source)
    target_rate = currencies.get(target)
    coefficient = target_rate / source_rate
    return round(amount * coefficient, 2)

def input_currency(input_message, currencies):
    currency = input(input_message).strip()
    while currency not in currencies:
        currency = input(f'Incorrect currency {currency}, enter the correct one: ').strip()
    return currency

print("Hi, this is currency converter")

exchange_rate = get_currencies()

print("""
Converter usage:
 - choose the source currency
 - choose the target currency
 - enter amount of source
Available currecies: 
""")
for currency in exchange_rate:
    print(f'- {currency}')


source_currency = input_currency(input_message="Enter source currency: ",
                                 currencies=exchange_rate)

target_currency = input_currency(input_message="Enter target currency: ",
                                 currencies=exchange_rate)

source_amount = float(input(f'enter amount of {source_currency}: '))
while type(source_amount) not float:
    source_amount = float(input(f'Incorrect type {type(source_amount)}, enter the numeric one: '))


target_amount = convert(source_amount, source_currency, target_currency, exchange_rate)
print(f'Result: {source_amount} {source_currency} = {target_amount} {target_currency}')

